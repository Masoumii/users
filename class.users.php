<?php

abstract class User{
	
	/* Global Variables */
	protected $usr = null;
	protected $passw = null;
	protected $loggedIn = null;
	
	/* Constructor */
	public function __construct($usr,$passw){
		$this->usr = $usr;
		$this->passw = $passw;
			}
	
	/* Getter Username */
	public function getUsr(){
		return $this->usr;
			}
	
		/* Getter Password */
	public function getPassw(){
		return $this->passw;
			}
	
	/* change password */
	public function changePassw($passw){
			if($this->loggedIn === true){
				$this->passw = sha1($passw);
				echo "Wachtwoord van ".$this->usr." is succesvol veranderd";
			}
	}
	
	/* Class method */
	public function login($enteredPassw){
			if($enteredPassw === $this->passw){
				$this->loggedIn = true;
				 echo $this->usr." is ingelogd<br>";
			}
	}				
}



/* Normal user class - subclass van User */
class NormalUser extends User{
	
	public function login($enteredPassw){
		User::login($enteredPassw);
			}
	
	public function changePassw($passw){
		User::changePassw($passw);
			}
	
	public function getPassw(){
		User::getPassw();
			}
	
		/* Constructor */
	public function __construct($usr, $passw){
			User::__construct($usr, $passw);
			}
	
		/* Class method - Protected omdat niet alleen de normal, maar ook supervisor users hier gebruik van (zouden) kunnen maken */
	protected function doNormalUserStuff(){
		// normaluser acties hier
			}
	
}


/* Supervisor class - subclass van NormalUser */
class Supervisor extends User{
	
			/* Constructor */
	public function __construct($usr, $passw){
		User::__construct($usr, $passw);
	}
	
	public function login($enteredPassw){
		User::login($enteredPassw);
			}
	
	public function changePassw($passw){
		User::changePassw($passw);
			}
	
		protected function doNormalUserStuff(){
		NormalUser::doNormalUserStuff();
	}
	
		/* Class method - Beschermd omdat alleen supervisor, supervisor stuff kan doen */
	private function doSuperviserStuff(){
		// Supervisor acties hier
		}

}

?>